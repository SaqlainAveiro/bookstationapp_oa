﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookomari.com.Models;
using DomainLayerOA.Models;
using RepositoryLayerOA;
using ServiceLayerOA.Service.Interface;
using ServiceLayerOA.Service.Implementation;

namespace Bookomari.com.Controllers
{
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }



        // GET: Author
        public IActionResult Index(string AuthorSearchString)
        {
            var authorList = new AuthorSearchViewModel();

            authorList.Authors = _authorService.GetAllAuthors();

            if (!String.IsNullOrEmpty(AuthorSearchString))
            {
                authorList.Authors = authorList.Authors.Where(b => b.AuthorName.Contains(AuthorSearchString));
            }

            authorList.AuthorSearchString = AuthorSearchString;

            return View(authorList);
        }




        // GET: Author/Details/5
        public IActionResult Details(int id)
        {
            if (id == null || _authorService.GetAllAuthors() == null)
            {
                return NotFound();
            }

            var author = _authorService.GetSingleAuthor(id);

            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }




        // GET: Author/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Author/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(AuthorEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var author = new Author();

                author.AuthorName = model.AuthorName;
                author.Address = model.Address;
                author.AuthorPhoto = model.GetImageBytes();

                _authorService.AddAuthor(author);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }




        // GET: Author/Edit/5
        public IActionResult Edit(int id)
        {
            // Find the author to edit
            Author author = _authorService.GetSingleAuthor(id);

            // If the author doesn't exist, return a 404 error
            if (author == null)
            {
                return NotFound();
            }

            string fileName = "author.jpg";

            AuthorEditViewModel viewModel;

            using (var stream = new MemoryStream(author.AuthorPhoto))
            {
                var file = new FormFile(stream, 0, stream.Length, null, fileName)
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "image/jpeg" // set the content type of your image here
                };

                // now you can use the IFormFile object as needed
                // Create a new view model for the edit form
                viewModel = new AuthorEditViewModel
                {
                    AuthorId = author.AuthorId,
                    AuthorName = author.AuthorName,
                    Address = author.Address,
                    AuthorPhoto = file
                };
            }
            return View(viewModel);
        }

        // POST: Author/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, AuthorEditViewModel model)
        {
            if (id != model.AuthorId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var author = _authorService.GetSingleAuthor(id);

                if (author == null)
                {
                    return NotFound();
                }

                author.AuthorName = model.AuthorName;
                author.Address = model.Address;
                author.AuthorPhoto = model.GetImageBytes();

                _authorService.UpdateAuthor(author);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }




        // GET: Author/Delete/5
        public IActionResult Delete(int id)
        {
            if (id == null || _authorService.GetAllAuthors() == null)
            {
                return NotFound();
            }

            var author = _authorService.GetSingleAuthor(id);
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }

        // POST: Author/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_authorService.GetAllAuthors() == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Authors'  is null.");
            }
            var author = _authorService.GetSingleAuthor(id);
            if (author != null)
            {
                _authorService.DeleteAuthor(author.AuthorId);
            }
            
            return RedirectToAction(nameof(Index));
        }

        private bool AuthorExists(int id)
        {
          return (_authorService.GetAllAuthors()?.Any(e => e.AuthorId == id)).GetValueOrDefault();
        }
    }
}
