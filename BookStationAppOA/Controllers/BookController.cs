﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Bookomari.com.Models;
using DomainLayerOA.Models;
using RepositoryLayerOA;
using ServiceLayerOA.Service.Interface;

namespace Bookomari.com.Controllers
{
    public class BookController : Controller
    {
        private readonly IBookService _bookService;
        private readonly IAuthorService _authorService;


        public BookController(IBookService bookService, IAuthorService authorService)
        {
            _bookService = bookService;
            _authorService = authorService;
        }


        [HttpGet]
        public IActionResult Index(string BookSearchString)
        {
            var bookList = new BookSearchViewModel();

            bookList.Books = _bookService.GetAllBooks();

            if (!String.IsNullOrEmpty(BookSearchString))
            {
                bookList.Books = bookList.Books.Where(b => b.BookName.Contains(BookSearchString));
            }

            bookList.BookSearchString = BookSearchString;

            return View(bookList);
        }



        public IActionResult Details(int id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var book = _bookService.GetSingleBook(id);

            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Book/Create
        public IActionResult Create()
        {
            // retrieve all authors from the database
            var authors = _authorService.GetAllAuthors();


            // create a new instance of the BookEditViewModel
            var model = new BookEditViewModel();

            // set the Authors property to a SelectList containing all authors
            ViewBag.Authors = new SelectList(authors, "AuthorId", "AuthorName");

            return View(model);
        }


        // POST: Book/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(BookEditViewModel model)
        {
            var author = _authorService.GetSingleAuthor(model.AuthorId);
            //model.Author = author;

            if (ModelState.IsValid)
            {
                var book = new Book();

                book.BookName = model.BookName;
                book.Language = model.Language;
                book.BookCoverPhoto = model.GetImageBytes();
                book.Author = author;
                book.AuthorId = model.AuthorId;

                _bookService.AddBook(book);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }


        [HttpGet]
        public IActionResult Edit(int id)
        {
            // Find the book to edit
            var book = _bookService.GetSingleBook(id);

            // If the book doesn't exist, return a 404 error
            if (book == null)
            {
                return NotFound();
            }

            string fileName = "book.jpg";

            BookEditViewModel viewModel;

            using (var stream = new MemoryStream(book.BookCoverPhoto))
            {
                var file = new FormFile(stream, 0, stream.Length, null, fileName)
                {
                    Headers = new HeaderDictionary(),
                    ContentType = "image/jpeg" // set the content type of your image here
                };

                // now you can use the IFormFile object as needed
                // Create a new view model for the edit form
                viewModel = new BookEditViewModel
                {
                    BookId = book.BookId,
                    BookName = book.BookName,
                    Language = book.Language,
                    CoverPhoto = file,
                    AuthorId = book.AuthorId
                };
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, BookEditViewModel model)
        {
            if (id != model.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var book = _bookService.GetSingleBook(id);

                if (book == null)
                {
                    return NotFound();
                }

                book.BookName = model.BookName;
                book.Language = model.Language;
                book.BookCoverPhoto = model.GetImageBytes();
                /*book.AuthorId = model.AuthorId;
                book.Author = model.Author;*/

                _bookService.UpdateBook(book);
                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }




        [HttpGet]
        public IActionResult Delete(int id)
        {
            if (id == null || _bookService.GetAllBooks() == null)
            {
                return NotFound();
            }

            var book = _bookService.GetSingleBook(id);

            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            if (_bookService.GetAllBooks() == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Books' is null.");
            }
            var book = _bookService.GetSingleBook(id);
            if (book != null)
            {
                _bookService.DeleteBook(id);
            }

            return RedirectToAction(nameof(Index));
        }

    }
}
