﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace Bookomari.com.Models
{
    public class AuthorEditViewModel
    {
        public int AuthorId { get; set; }
        [Required]
        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }

        public string Address { get; set; }

        [Display(Name = "Author Photo")]
        public IFormFile AuthorPhoto { get; set; }

        public byte[] GetImageBytes()
        {
            using var stream = new MemoryStream();
            AuthorPhoto.CopyTo(stream);
            return stream.ToArray();
        }
    }
}
