﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace Bookomari.com.Models
{
    public class BookEditViewModel
    {
        public int BookId { get; set; }
        [Display(Name = "Book Name")]
        public string BookName { get; set; }
        [Display(Name = "Book Language")]
        public string Language { get; set; }

        [Display(Name = "Book Cover Photo")]
        public IFormFile CoverPhoto { get; set; }

        public byte[] GetImageBytes()
        {
            using var stream = new MemoryStream();
            CoverPhoto.CopyTo(stream);
            return stream.ToArray();
        }
        //public Author Author { get; set; }
        public int AuthorId { get; set; }

    }
}
