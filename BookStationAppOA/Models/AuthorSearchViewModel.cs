﻿using System.ComponentModel;
using DomainLayerOA.Models;

namespace Bookomari.com.Models;

public class AuthorSearchViewModel
{
    [DisplayName("")]
    public string AuthorSearchString { get; set; }
    public IEnumerable<Author> Authors { get; set; }
}
