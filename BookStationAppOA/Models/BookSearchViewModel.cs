﻿using System.ComponentModel;
using DomainLayerOA.Models;

namespace Bookomari.com.Models;

public class BookSearchViewModel
{
    [DisplayName("")]
    public string BookSearchString { get; set; }
    public IEnumerable<Book> Books { get; set; }
}
