﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayerOA.Models;

namespace ServiceLayerOA.Service.Interface
{
    public interface IBookService
    {
        //Get All books
        IEnumerable<Book> GetAllBooks();

        //Get Single book
        Book GetSingleBook(int id);

        //Add book
        void AddBook(Book book);

        //Update or Edit book
        void UpdateBook(Book book);

        //Delete book
        void DeleteBook(int id);
    }
}
