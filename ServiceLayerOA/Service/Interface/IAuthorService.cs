﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainLayerOA.Models;

namespace ServiceLayerOA.Service.Interface
{
    public interface IAuthorService
    {
        //Get All Authors
        IEnumerable<Author> GetAllAuthors();

        //Get Single Author
        Author GetSingleAuthor(int id);

        //Add Author
        void AddAuthor(Author author);

        //Update or Edit Author
        void UpdateAuthor(Author author);

        //Delete Author
        void DeleteAuthor(int id);
    }
}
