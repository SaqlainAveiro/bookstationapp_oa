﻿using DomainLayerOA.Models;
using RepositoryLayerOA;
using ServiceLayerOA.Service.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayerOA.Service.Implementation
{
    public class AuthorService : IAuthorService
    {
        private readonly ApplicationDbContext _context;

        public AuthorService(ApplicationDbContext _context)
        {
            this._context = _context;
        }

        public IEnumerable<Author> GetAllAuthors()
        {
            return _context.Authors.Include(b => b.Books).ToList();
        }

        Author IAuthorService.GetSingleAuthor(int id)
        {
            return _context.Authors.Include(b => b.Books).FirstOrDefault(m => m.AuthorId == id);
        }

        void IAuthorService.AddAuthor(Author author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
        }

        void IAuthorService.UpdateAuthor(Author author)
        {
            var selectedAuthor = _context.Authors.Find(author.AuthorId);
            if (selectedAuthor != null)
            {
                _context.Authors.Update(author);
                _context.SaveChanges();
            }
        }

        void IAuthorService.DeleteAuthor(int id)
        {
            var author = _context.Authors.Find(id);
            if (author != null)
            {
                _context.Authors.Remove(author);
                _context.SaveChanges();
            }
        }
    }
}
