﻿using DomainLayerOA.Models;
using RepositoryLayerOA;
using ServiceLayerOA.Service.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayerOA.Service.Implementation
{
    public class BookService : IBookService
    {
        private readonly ApplicationDbContext _context;

        public BookService(ApplicationDbContext _context)
        {
            this._context = _context;
        }
        IEnumerable<Book> IBookService.GetAllBooks()
        {
            return _context.Books.Include(b => b.Author).ToList();
        }

        Book IBookService.GetSingleBook(int id)
        {
            return _context.Books.Include(b => b.Author).FirstOrDefault(m => m.BookId == id);
        }

        void IBookService.AddBook(Book book)
        {
            _context.Books.Add(book);
            _context.SaveChanges();
        }
        

        void IBookService.UpdateBook(Book book)
        {
            var selectedBook = _context.Books.Find(book.BookId);
            if (selectedBook != null)
            {
                _context.Books.Update(book);
                _context.SaveChanges();
            }
        }

        void IBookService.DeleteBook(int id)
        {
            var book = _context.Books.Find(id);
            if (book != null)
            {
                _context.Books.Remove(book);
                _context.SaveChanges();
            }
        }
    }
}
